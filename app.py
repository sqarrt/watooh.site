from flask import Flask, render_template, request, jsonify, url_for
from nn.utils import get_binary_tone_classifier, get_fasttext_compressed
import yaml

application = Flask(__name__)


@application.route('/')
def main_page():
    return 'Hello, world!'


@application.route('/friends')
def friends_page():
    return render_template("index.html", title='Projects')


def predict_sentence(expr: str) -> bool:
    model = get_binary_tone_classifier()
    res = model.predict([expr, ]).item(0)
    return res


def predict_sentence_proba(expr: str) -> bool:
    model = get_binary_tone_classifier()
    res = model.predict_proba([expr, ]).item(0)
    res = 1 - round(res, 2)
    return res


@application.route('/api/get_tone_class', methods=['GET', 'POST'])
def get_tone_class():
    sentence = request.args.get('sentence', '', type=str)

    result = predict_sentence(sentence)

    return jsonify(result=result)


@application.route('/api/get_tone_proba', methods=['GET', 'POST'])
def get_tone_proba():
    sentence = request.args.get('sentence', '', type=str)

    result = predict_sentence_proba(sentence)

    return jsonify(result=result)


@application.route('/api/get_tone_smile_url', methods=['GET', 'POST'])
def get_tone_smile_url():
    proba = request.args.get('proba', '', type=float)

    score = int(proba * 10) + 1
    score = 10 if score > 10 else score

    return jsonify(result=url_for('static', filename=f'images/emoji_{score}.png'))


with open('static/smile_x_url.yaml','rb') as file:
    smile_urls = yaml.safe_load(file)


@application.route('/api/get_closest_smile_url', methods=['GET', 'POST'])
def get_closest_smile_url():
    ftc = get_fasttext_compressed()
    sentence = request.args.get('sentence', 'пусто', type=str)

    smile = sorted(smile_urls.items(), key=lambda tpl: ftc.similarity(tpl[0], sentence))[-1]

    return jsonify(result=smile[-1])


@application.route('/tone')
def tone():
    return render_template("tone.html", title='Projects')


if __name__ == "__main__":
    application.run(host='0.0.0.0')
