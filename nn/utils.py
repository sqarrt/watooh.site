import compress_fasttext
from gensim import matutils
from gensim.parsing.preprocessing import preprocess_string, strip_multiple_whitespaces, remove_stopwords, stem_text
from numpy import dot
import pickle

models = {
    'binary_tone_classifier': None,
    'compressed_fasttext': None,
}


class FTC:
    def __init__(self, model):
        self.model = model

    def vec(self, txt):
        words = preprocess_string(txt,
                                  filters=[strip_multiple_whitespaces,
                                           remove_stopwords,
                                           stem_text])
        if not words:
            words.append('')
        return sum(map(lambda word: self.model[word], words))

    def cosine(self, vec1, vec2):
        return dot(matutils.unitvec(vec1), matutils.unitvec(vec2))

    def similarity(self, txt1, txt2):
        return self.cosine(self.vec(txt1), self.vec(txt2))


def get_binary_tone_classifier():
    name = 'binary_tone_classifier'
    bts = models.get(name)
    if not bts:
        with open(f'nn/models/{name}.p', 'rb') as file:
            bts = pickle.load(file)
            models[name] = bts
    return bts


def get_fasttext_compressed():
    name = 'fasttext_compressed'
    ftc = models.get(name)
    if not ftc:
        ftc = compress_fasttext.models.CompressedFastTextKeyedVectors.load(f'nn/models/{name}.model')
        models[name] = ftc
    return FTC(ftc)


if __name__ == '__main__':
    ftc = get_fasttext_compressed()
    word1, word2 = 'мама', 'папа'
    print(f'Сходность {word1} и {word2}:', ftc.similarity('мама', 'папа'))
