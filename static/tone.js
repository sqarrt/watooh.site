function remind_adblock() {
    $("#emoji_title").text("сначала отключи адблок");
}

function get_request(method, payload, callback) {
    let url = $SCRIPT_ROOT + '/api/' + method;
    let request = $.getJSON(url, payload);
    request.done(callback);
    request.fail(remind_adblock);
}

function get_smile_url_callback(response) {
    let sentence = $("#sentence")
    let emoji = $("#emoji")
    let emoji_title = $("#emoji_title")

    emoji_title.removeClass("text-muted")
    emoji_title.text(sentence.val())
    emoji.attr("src", response.result);
    sentence.val("");
}

function get_tone_proba_callback(response) {
    get_request('get_tone_smile_url',
        {proba: response.result},
        get_smile_url_callback)
}

function smile_loading() {
    $("#emoji").attr("src", '../static/images/loading.gif')
}

function open_image_in_new_tab() {
    html2canvas(document.querySelector("#emoji_div")).then(function (canvas) {
        let dataURL = canvas.toDataURL("image/png");
        let new_window = window.open(dataURL, '_blank');
        new_window.document.write('<img src="' + dataURL + '"/>');
    });
}

$(document).on('click', '#emoji', open_image_in_new_tab);

$(document).keypress(function (event) {
    let keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        smile_loading();
        get_request('get_closest_smile_url',
            {sentence: $('#sentence').val()},
            get_smile_url_callback)
    }
});
